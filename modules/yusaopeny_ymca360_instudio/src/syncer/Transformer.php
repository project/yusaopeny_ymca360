<?php

namespace Drupal\yusaopeny_ymca360_instudio\syncer;

use Drupal\yusaopeny_ymca360\syncer\TransformerBase;
use Drupal\yusaopeny_ymca360\syncer\TransformerInterface;

/**
 * Processes data received from YMCA360 API.
 *
 * @package Drupal\yusaopeny_ymca360.
 */
class Transformer extends TransformerBase implements TransformerInterface {

}
